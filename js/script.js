//Define variables
let playing = false;
let startReset = document.getElementById("startReset");
let player1Positions = [];
let player2Positions = [];
let playerOne = false;
let playerTwo = false;
let playerOneScore = "0";
let playerTwoScore  = "0";
let player = document.getElementById("player");
let computer = document.getElementById("computer");
let boxes = document.querySelector('#playing-field');
let selectOpponent = document.getElementById("selectOpponent");
let vsPlayer = false;
let winner = document.getElementById("winner");


startReset.addEventListener('click', startResetGame);

player.addEventListener('click', versus);

boxes.addEventListener('click', addMark)
computer.addEventListener('click', versus);

function setText(id, text) {
    document.getElementById(id).innerHTML = text;
}

function show(id) {
    document.getElementById(id).style.display = 'block';
}

function hide(id) {
    document.getElementById(id).style.display = 'none';
}

function startResetGame(e) {
    player1Positions = [];
    player2Positions = [];
    playerOneScore ="0";
    playerTwoScore = "0";
    setText("playerOneScore",playerOneScore);
    setText("playerTwoScore",playerTwoScore);
    if (playing === true) {
        //game is on and you want to reset
        setText("startReset", "Start Game!");
        for (let i = 1; i < 10; i++)
            setText("box" + i, '');
  
        setText("gameover", "<p>Game</p><p>Over </p>");
        hide("selectOpponent");
        show("gameover");


    } else {
        //game is off and you want to start a new game
        setText("startReset", 'Reset Game!');
        hide("gameover");
        show("selectOpponent");

        playerOne = true;
        playerTwo = false;

    }
    playing = !playing;
}
function versus(e) {
    if (e.target.id === 'player') {
        vsPlayer = true;
        hide("selectOpponent");

        for (let i = 1; i < 10; i++)
            setText("box" + i, '');
} else if (e.target.id === 'computer') {

        vsPlayer = false;
        hide("selectOpponent");
        for (let i = 1; i < 10; i++)
            setText("box" + i, '');
    }
}

function addMark(e) {
    console.log(vsPlayer);
    if (vsPlayer == true) {

        if (playerOne && e.target.innerHTML === '' && selectOpponent.style.display === 'none') {
            e.target.innerHTML = 'X';
            playerOne = !playerOne;
            playerTwo = !playerTwo;
            player1Positions.push(e.target.getAttribute("id"));
            checkIfGameOver();


        } else if (playerTwo && e.target.innerHTML === '' && selectOpponent.style.display === 'none' && gameover.style.display === 'none') {
            e.target.innerHTML = 'O';
            playerTwo = !playerTwo;
            playerOne = !playerOne;
            player2Positions.push(e.target.getAttribute("id"));
            checkIfGameOver();
        }
    } else {
        if (playerOne && e.target.innerHTML === '' && selectOpponent.style.display === 'none'  && gameover.style.display === 'none') {   
            e.target.innerHTML = 'X';
            player1Positions.push(e.target.getAttribute("id"));
            computerMove();
            checkIfGameOver();
        }
    }

}

function computerMove() {
    if (selectOpponent.style.display === 'none' && (player1Positions.length + player2Positions.length < 9) && !checkIfGameOver() ) {

        let boxNumber;

        do {

            boxNumber = String(Math.round((1 + Math.random() * 8)));
        } while (document.getElementById("box" + boxNumber).innerHTML != '');

        setText("box" + boxNumber, "O");
      
        player2Positions.push("box"+boxNumber);
    }
}

function checkIfGameOver() {

    if ((player1Positions.includes("box1") && player1Positions.includes("box2") && player1Positions.includes("box3")) ||
        (player1Positions.includes("box1") && player1Positions.includes("box4") && player1Positions.includes("box7")) ||
        (player1Positions.includes("box1") && player1Positions.includes("box5") && player1Positions.includes("box9")) ||
        (player1Positions.includes("box7") && player1Positions.includes("box5") && player1Positions.includes("box3")) ||
        (player1Positions.includes("box2") && player1Positions.includes("box5") && player1Positions.includes("box8")) ||
        (player1Positions.includes("box4") && player1Positions.includes("box5") && player1Positions.includes("box6")) ||
        (player1Positions.includes("box8") && player1Positions.includes("box7") && player1Positions.includes("box9")) ||
        (player1Positions.includes("box3") && player1Positions.includes("box6") && player1Positions.includes("box9"))


    ) {
        setText("gameover","<p>Player 1</p><p>Wins!</p>");
        show("gameover");
        playerOneScore = "1";
        setText("playerOneScore",playerOneScore);
        for (let i = 1; i < 10; i++)
            setText("box" + i, '');
        return true;
        
    } else if ((player2Positions.includes("box1") && player2Positions.includes("box2") && player2Positions.includes("box3")) ||
        (player2Positions.includes("box1") && player2Positions.includes("box4") && player2Positions.includes("box7")) ||
        (player2Positions.includes("box1") && player2Positions.includes("box5") && player2Positions.includes("box9")) ||
        (player2Positions.includes("box7") && player2Positions.includes("box5") && player2Positions.includes("box3")) ||
        (player2Positions.includes("box2") && player2Positions.includes("box5") && player2Positions.includes("box8")) ||
        (player2Positions.includes("box4") && player2Positions.includes("box5") && player2Positions.includes("box6")) ||
        (player2Positions.includes("box8") && player2Positions.includes("box7") && player2Positions.includes("box9")) ||
        (player2Positions.includes("box3") && player2Positions.includes("box6") && player2Positions.includes("box9"))
    ) {
        if(vsPlayer === true){
            setText("gameover","<p>Player 2</p><p>Wins!</p>");
        }else{
            setText("gameover","<p>Computer</p><p>Wins!</p>")
        }
        show("gameover");
        playerTwoScore = "1";
        setText("playerTwoScore",playerTwoScore);
        for (let i = 1; i < 10; i++)
            setText("box" + i, '');
        return true;

    } else if (player1Positions.length + player2Positions.length == 9) { 
        setText("gameover","<p>Draw!</p>");
        show("gameover");
        for (let i = 1; i < 10; i++)
            setText("box" + i, '');
        return true;
    }
    
}